module gui;

import gio.Application : GioApplication = Application;

import gtk.Application;
import gtk.ApplicationWindow;
import gtk.Builder;

import gtk.Button;
import gtk.Widget;
import gtk.ComboBoxText;
import gtk.MenuItem;
import gtk.MenuItemAccessible;

import glib.GException;

import std.stdio;
import core.stdc.stdlib;
import std.string;

// Imports locaux
import callbacks;

void createGUI(Application appli, string gladeFile)
{
    auto builder = new Builder();
    try
    {
        builder.addFromFile(gladeFile);
    }
    catch (GException expt)
    {
        writeln("Oops, could not create Glade object, check your glade file ;)");
    }

    auto main7z = cast(ApplicationWindow) builder.getObject("main7z");
    if (main7z !is null)
    {
        main7z.setApplication(appli);
        // Catch le bouton de fermeture sur la focntion de sortie
        main7z.addOnDestroy(delegate void(Application) { onExit(appli); });
        // Exit
        auto exitMenu = cast(MenuItem) builder.getObject("menu_exit");
        if (exitMenu !is null)
        {
            exitMenu.addOnActivate(delegate void(MenuItem) { onExit(appli); });
        }
        // Dir UP
        auto buttonDirUp = cast(Button) builder.getObject("ButtonDirUp");
        if (buttonDirUp !is null)
        {
            buttonDirUp.addOnClicked(delegate void(Button aux) { onDirUp(); });
        }
        // Combo
        auto comboBoxPath = cast(ComboBoxText) builder.getObject("ComboPath");
        if (comboBoxPath !is null)
        {
            addComboPath(comboBoxPath);
            comboBoxPath.addOnChanged(delegate void(ComboBoxText aux) { addComboPath(comboBoxPath); });
        }
        builder.connectSignals(null);
        // File Open
        auto openMenu = cast(MenuItem) builder.getObject("menu_open");
        if (openMenu !is null)
        {
            openMenu.addOnActivate(delegate void(MenuItem) { onOpen(appli); });
        }

        main7z.showAll();
    }
    else
    {
        writeln("No window?");
        exit(EXIT_FAILURE);
    }
}
