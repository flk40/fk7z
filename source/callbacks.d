module callbacks;

import gtk.Application;
import gio.Application : GioApplication = Application;
import gdk.Event;
import gtk.ComboBoxText;
import gtk.FileChooserDialog;

import std.stdio;
import core.stdc.stdlib;
import std.string;

void addComboPath(ComboBoxText combo)
{
    if (combo !is null)
    {
        combo.appendText("part 1");
    }
}

void onOpen(Application appli)
{
    writeln("Open");
    FileChooserDialog dialog = new FileChooserDialog("Open a File", null, FileChooserAction.OPEN, null, null);
    int response = dialog.run();

    if(response == ResponseType.OK)
    {
        string filename = dialog.getFilename();
        writeln(filename);
    }
    else
    {
        writeln("cancelled.");
    }

    dialog.destroy();
}

void onExit(Application appli)
{
    writeln("Exit");
    appli.quit();
}

void onDirUp()
{
    writeln("Dir UP");
}
