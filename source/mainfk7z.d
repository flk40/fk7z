module source.mainfk7z;

import gio.Application : GioApplication = Application;

import gtk.Application;

// Imports locaux
import callbacks;
import gui;

int main(string[] argv)
{
	string gladefile = "resources/gui/main7z.glade";

	auto application = new Application("org.gtkd.fk7z", GApplicationFlags.FLAGS_NONE);

	void buildAndDisplay(GioApplication a)
	{
		createGUI(application, gladefile);
	}

	application.addOnActivate(&buildAndDisplay);
	return application.run(argv);
}
