Date:    2015/08/03
Author:  masamunecyrus
Website: https://masamunecyrus.deviantart.com

This has been tested on Windows 10 with 7-Zip 15.05 beta (64-bit) and
7-Zip Theme Manager 2.1.1.

7-Zip is available, here:
http://www.7-zip.org/

7-Zip Theme Manager is available, here:
http://www.7ztm.de/


----------------
| Installation |
----------------

Extract the files into your main 7-Zip Theme Manager directory.

Your folder structure should be as follows:

7zTM
|-- 7zTM.exe
|-- filetype
`-- toolbar
    `-- Office 2013
        |-- preview.jpg
        |-- theme.ini
        |-- 24x24
        |   |-- Add.bmp
        |   |-- Copy.bmp
        |   |-- Delete.bmp
        |   |-- Extract.bmp
        |   |-- Info.bmp
        |   |-- Move.bmp
        |   `-- Test.bmp
        `-- 48x36
            |-- Add.bmp
            |-- Copy.bmp
            |-- Delete.bmp
            |-- Extract.bmp
            |-- Info.bmp
            |-- Move.bmp
            |-- Test.bmp
